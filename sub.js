const mqtt = require('mqtt');
const devID = 'INEM_DEMO';
const topic = `devicesIn/${devID}/data`;

const Influx = require('influx');

const influx = new Influx.InfluxDB({
    host: 'localhost',
    database: 'checkInflux',
    username: "kaizen",
    password: "influx@123",
});

const client = mqtt.connect('mqtt://localhost:1883');

client.on('connect', ()=>{
    client.subscribe(topic);
    console.log('client 1 has subscribed successfully');
});



client.on('message', (topic, message)=>{
    // console.log('message received by client 1', message.toString());
    const pubData = JSON.parse(message);
    // console.log(pubData);
    for(let i = 0; i < pubData.data.length; i++){
        // console.log('checking loop ', i);
        influx.writePoints([

            {
        
                measurement: pubData.device,
        
                tags: {sensor: pubData.data[i].tag},
        
                fields: {value: pubData.data[i].value},
        
                timestamp: pubData.time,
            },
        
        ])
        
            .then(() => {
        
                console.log("Data saved to InfluxDB");
        
            })
        
            .catch((err) => {
        
                console.error("Error writing data to InfluxDB:", err);
        
            });
    }
    
})
